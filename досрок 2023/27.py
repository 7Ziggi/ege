for file in ['27A.txt', '27B.txt']:
    with open(file) as f:
        n = int(f.readline())
        k = int(f.readline())
        nums = [int(f.readline()) for _ in range(k)]
        # количество не может быть отрицательным
        mx_left = mx = 0
        for i in range(k, n):
            x = int(f.readline())
            # переопределяем максимум
            # в начале последовательности
            mx_left = max(mx_left, nums[i % k])
            # переопеделяем максимум если надо
            mx = max(mx, x + mx_left)
            # на место числа на K левее записываем х
            nums[i % k] = x
            # выводим максимум
        print(file, mx)
