def F(n):
    return n + 3 + F(n + 3) if n < 2025 else n


print(F(23) - F(21))
