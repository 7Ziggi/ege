f = open('09.txt')
c = 0
strings = [list(map(int, s.split())) for s in f]


def F1(l: list):
    for i in set(l):
        if l.count(i) > 1:
            return False
    return True


def F2(l: list):
    ma = max(l)
    l.remove(ma)
    mi = min(l)
    l.remove(mi)
    return (3 * (ma + mi)) <= (2 * sum(l))


for s in strings:
    if F1(s) and F2(s):
        c += 1
print(c)
