for i in range(101, 10 ** 20):
    x = i
    l = 2 * x - 30
    m = 2 * x + 30
    while l != m:
        if l > m:
            l = l - m
        else:
            m = m - l
    if m == 30:
        print(i)
        break
