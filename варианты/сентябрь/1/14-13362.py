i = 125 + 25 ** 3 + 5 ** 9
s = ''
while i > 0:
    s += str(i % 5)
    i //= 5
s = s[::-1]
print(s.count('0'))
