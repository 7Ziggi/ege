f = open('txt\\24-35482.txt').readlines()

min_g = 10 ** 20
c_g = 10 ** 20
s_g = ''
alph = [chr(i) for i in range(65, 91)]
c_alph = [0] * 26
g = 10 ** 20

for s in f:
    scg = s.count('G')
    g = min(g, scg)
    if scg < c_g:
        c_g = scg
        s_g = s

for i in range(len(alph)):
    letter = alph[i]
    c_alph[i] = s_g.count(letter)

max_c_alph = max(c_alph)
max_c_alph_index = 0

for i in range(len(c_alph) - 1, -1, -1):
    if c_alph[i] == max_c_alph:
        max_c_alph_index = i
        break

print(alph[max_c_alph_index])
