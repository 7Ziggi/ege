f = open("txt\\24-36879.txt").read().split()


def F(s):
    alphabet = sorted("QWERTYUIOPASDFGHJKLZXCVBNM")
    c_alphabet = [0] * 26
    for l in alphabet:
        first = s.index(l)
        last = len(s) - 1 - s[::-1].index(l)
        c_alphabet[alphabet.index(l)] = last - first
    return c_alphabet


max_distance = -10 ** 20
for s in f:
    if s.count("G") < 25:
        max_distance = max(max_distance, max(F(s)))

print(max_distance)
