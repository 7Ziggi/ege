f = open('txt\\24-35913.txt').read().split()
min_N = 10 ** 20
s = ''
alphabet = sorted('QWERTYUIOPASDFGHJKLZXCVBNM', reverse=True)
c_alphabet = [0] * 26

for i in f:
    count_n = i.count('N')
    if count_n < min_N:
        min_N = count_n
        s = i

for i in range(len(alphabet)):
    c_alphabet[i] = s.count(alphabet[i])

index_often_letter = c_alphabet.index(max(c_alphabet))
print(alphabet[index_often_letter])
