f = open('txt\\24-35482.txt').read().split()
s = ''
min_G = 10 ** 20

alphabet = sorted('QWERTYUIOPASDFGHJKLZXCVBNM', reverse=True)
c_alphabet = [0] * 27

for i in f:
    count_G = i.count('G')
    if count_G < min_G:
        min_G = count_G
        s = i

for i in range(len(alphabet)):
    c_alphabet[i] = s.count(alphabet[i])

index_often_letter = c_alphabet.index(max(c_alphabet))
print(alphabet[index_often_letter])
