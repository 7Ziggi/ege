f = open("txt\\24-33769.txt").readline()
alph = sorted(set(f))
c_alph = [0] * len(alph)
for i in range(len(f) - 2):
    if f[i] == f[i + 1]:
        next_letter = f[i + 2]  # следующая за двумя одинаковыми буквами буква
        index_next_letter = alph.index(next_letter)  # индекс этой буквы
        c_alph[index_next_letter] += 1  # увеличиваем на 1 кол-во этой буквы
print(alph[c_alph.index(max(c_alph))])
