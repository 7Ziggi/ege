def f(x):
    d = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
    return sorted(d)


c = 0
for i in range(300_000_001, 10 ** 20):
    if c >= 5:
        break
    d = f(i)
    if len(d) >= 5:
        print(i, d[-5])
        c += 1
