def f(x):
    d = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
    return sorted(d)

c = 1
for i in range(460_000_001, 10 ** 20):
    if c > 5:
        break
    m = f(i)
    if len(m) >= 5:
        print(m[-5])
        c += 1
