def f(x):
    d = set()
    for i in range(1, int(x ** 0.5) + 1):
        if x % i == 0:
            if i % 2 == 0:
                d.add(i)
            k = x // i
            if k % 2 == 0:
                d.add(k)
        if len(d) > 4:
            break
    return sorted(d)


for i in range(110203, 110246):
    d = f(i)
    if len(d) == 4:
        print(*d)
