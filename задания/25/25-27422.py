def f(x):
    d = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            d.add(i)
            k = x // i
            d.add(k)
        if len(d) > 2:
            break
    return sorted(d)


a = []
for i in range(174457, 174506):
    b = []
    d = f(i)
    if len(d) == 2:
        b += [d[0] * d[1]]
        b += d
        a += [b]
a.sort()
for i in a:
    print(i[1], i[2])
