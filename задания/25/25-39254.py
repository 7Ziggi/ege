def f(x):
    d = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
    d.add(x)
    return sorted(d)


c = 1
for i in range(500_000_001, 10 ** 20):
    if c > 5:
        break
    d = f(i)
    if len(d) >= 5:
        p = d[0] * d[1] * d[2] * d[3] * d[4]
        if 0 < p < i:
            print(p)
            c += 1
