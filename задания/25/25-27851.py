def f(x):
    d = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
        if len(d) > 4:
            break
    return sorted(d)


for i in range(210_235, 210_301):
    d = f(i)
    if len(d) == 4:
        print(*d)