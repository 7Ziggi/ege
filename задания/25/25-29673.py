def f(x):
    d = set()
    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
        if len(d) > 3:
            break
    return sorted(d)


for i in range(123456789, 223456790):
    d = f(i)
    if len(d) == 3:
        print(i, d[-1])
# 131079601 1225043
# 141158161 1295029
# 163047361 1442897
