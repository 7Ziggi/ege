for i in range(1, 10000):
    n = i
    bin_n = bin(n)[2:]
    c1 = bin_n.count('1')
    c0 = bin_n.count('0')
    if c1 > c0:
        bin_n += '1'
    else:
        bin_n += '0'
    r = int(bin_n, 2)
    if r > 80:
        print(r)
        break
