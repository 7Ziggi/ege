for i in range(0, 256):
    n = i
    bin_n = bin(n)[2:]
    bin_n = '0' * (8 - len(bin_n)) + bin_n

    bin_n = bin_n.replace('0', '*')
    bin_n = bin_n.replace('1', '0')
    bin_n = bin_n.replace('*', '1')
    r = int(bin_n, 2)
    if r - i == 111:
        print(i)
        break
