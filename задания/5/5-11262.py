for i in range(1000, 10000):
    n = str(i)
    i12 = int(n[0]) + int(n[1])
    i23 = int(n[1]) + int(n[2])
    i34 = int(n[2]) + int(n[3])

    max1 = max(i12, i23, i34)
    max2 = i12 + i23 + i34 - max1 - min(i12, i23, i34)
    s = str(max2) + str(max1)
    if s == '1517':
        print(n)
        break
