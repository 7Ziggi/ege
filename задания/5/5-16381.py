for i in range(1, 99999999):
    n = i
    bin_n = bin(i)[2:-1]
    if n % 2 == 1:
        bin_n += '10'
    else:
        bin_n += '01'
    r = int(bin_n, 2)
    if r == 2018:
        print(n)
        break
