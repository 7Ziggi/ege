for i in range(2, 10 ** 20):
    n = i
    bin_n = bin(n)[2:]
    for _ in range(3):

        c0 = bin_n.count('0')
        c1 = bin_n.count('1')

        if c0 == c1:
            bin_n += bin_n[-1]
        else:

            if c0 < c1:
                bin_n += '0'
            else:
                bin_n += '1'

    r = int(bin_n, 2)
    if (n > 104) and (r % 4 == 0):
        print(n)
        break
