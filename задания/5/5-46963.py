for i in range(2, 10 ** 20):
    n = i
    bin_n = bin(n)[2:]
    c1 = 0
    c0 = 0
    for j in range(len(bin_n)):
        if ((j + 1) % 2 == 0) and (bin_n[j] == '1'):
            c1 += 1
        elif ((j + 1) % 2 == 1) and (bin_n[j] == '0'):
            c0 += 1
    r = abs(c0 - c1)
    if r == 5:
        print(n)
        break