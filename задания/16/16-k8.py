def F(n):
    if n == 1:
        return 1
    if n == 2:
        return 2
    if n > 2 and n % 2 == 0:
        return int((n + F(n - 2)) / 5)
    if n > 2 and n % 2 != 0:
        return int((2 * n + F(n - 1) + F(n - 2)) / 4)


print(F(50))
