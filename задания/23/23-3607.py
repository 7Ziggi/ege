def calculator(start, finish):
    if start > finish:
        return 0
    if start == finish:
        return 1
    return calculator(start + 2, finish) + calculator(start * 5, finish)


print(calculator(2, 50))
