def f(start, finish):
    s = str(start)
    s = int(str(int(s[0]) + 1) + s[1:])
    if start > finish:
        return 0
    if start == finish:
        return 1
    return f(start + 1, finish) + f(s, finish)


print(f(35, 57))
