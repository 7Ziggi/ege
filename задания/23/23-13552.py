def f(s, fd):
    if s > fd:
        return 0
    if s == fd:
        return 1
    return f(s + 1, fd) + f(s + 2, fd) + f(s + 4, fd)


print(f(1, 8) * f(8, 15))
