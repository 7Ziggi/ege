def F(x, y):
    if x > y or x == 29:
        return 0
    if x == y:
        return 1
    return F(x + 1, y) + F(x * 2, y) + F(x * 3, y)


print(F(2, 13) * F(13, 44))
