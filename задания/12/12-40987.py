min_i = 999999999999999999999
min_len = 999999999999999999999
for i in range(201, 10001):
    s = '1' * i
    while '1111' in s:
        s = s.replace('1111', '22', 1)
        s = s.replace('222', '1', 1)
    min_i = min(min_i, s.count('1'))
    if min_i == s.count('1'):
        print(i)
        break
