for i1 in range(1, 101):
    for i2 in range(1, 101):
        for i3 in range(1, 101):
            s = '0' + '1' * i1 + '2' * i2 + '3' * i3 + '0'
            while not ('00' in s):
                s = s.replace('01', '210')
                s = s.replace('02', '3101')
                s = s.replace('03', '2012')
            if (s.count('1') == 70) and (s.count('2') == 56) and (s.count('3') == 23):
                print(i1 + i2 + i3 + 2)
                break
