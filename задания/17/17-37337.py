f = [int(i) for i in open("17-37337.txt")]
c = 0
max_sum = - 10 ** 20
# решено для последовательных пар
for i in range(len(f) - 1):
    e1 = f[i]
    e2 = f[i + 1]
    if (e1 % 160 != e2 % 160) and (e1 % 7 == 0 or e2 % 7 == 0):
        c += 1
        max_sum = max(max_sum, e1 + e2)
print(c, max_sum)
