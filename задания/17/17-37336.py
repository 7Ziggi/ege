f = [int(i) for i in open("txt\\17-37336.txt")]
c = 0
max_sum = - 10 ** 20
for i in range(len(f) - 1):
    e1 = f[i]
    e2 = f[i + 1]
    if e1 % 3 == 0 or e2 % 3 == 0:
        c += 1
        max_sum = max(max_sum, e1 + e2)
print(c, max_sum)
