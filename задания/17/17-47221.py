f = [int(i) for i in open('txt\\17-47221.txt')]
c = 0
max_sum = - 10 ** 20
m3 = - 10 ** 20

for i in f:
    if i % 10 == 3:
        m3 = max(m3, i)

for i in range(len(f) - 1):
    e1 = f[i]
    e2 = f[i + 1]
    if ((str(e1)[-1] == '3') and (str(e2)[-1] != '3')) or ((str(e1)[-1] != '3') and (str(e2)[-1] == '3')):
        sum_sq = e1 ** 2 + e2 ** 2
        if sum_sq >= m3 ** 2:
            c += 1
            max_sum = max(max_sum, sum_sq)

print(c, max_sum)
