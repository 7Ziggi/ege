f = [int(i) for i in open('txt\\17-46975.txt')]

c = 0
max_sum = - 10 ** 20

c_ch = 0
sum_ch = 0
sr_ch = 0

for i in f:
    if i % 2 == 0:
        c_ch += 1
        sum_ch += i
sr_ch = sum_ch / c_ch

for i in range(len(f) - 1):
    e1 = f[i]
    e2 = f[i + 1]
    if ((e1 % 3 == 0) and (e2 < sr_ch)) or ((e2 % 3 == 0) and (e1 < sr_ch)):
        c += 1
        max_sum = max(max_sum, e1 + e2)

print(c, max_sum)
