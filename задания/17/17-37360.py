f = [int(i) for i in open("txt\\17-37360.txt")]
c = 0
max_sum = - 10 ** 20
for i in range(len(f)):
    for j in range(i + 1, len(f)):
        el1 = f[i]
        el2 = f[j]
        if (el1 + el2) % 120 == 0:
            c += 1
            max_sum = max(max_sum, el1 + el2)
print(c, max_sum)
