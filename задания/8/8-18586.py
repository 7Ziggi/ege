from itertools import *

c = 0
for i in product('СВЕТА', repeat=5):
    s = ''.join(i)
    if s.count('С') >= 1:
        c += 1
print(c)
