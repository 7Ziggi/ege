from itertools import *

iskl = []
a = set()
for i in product('ОА', repeat=2):
    s = ''.join(i)
    iskl += [s]
for i in product('РСМХ', repeat=2):
    s = ''.join(i)
    iskl += [s]
for i in permutations('РОСОМАХА', r=8):
    s = ''.join(i)
    if all(j not in s for j in iskl):
        a.add(s)
print(len(a))
