from itertools import product

c = 0
for i in product('БОРИС', repeat=6):
    s = ''.join(i)
    if s.count('Б') == 1 and s.count('Р') == 1 and s.count('С') <= 1:
        c += 1
print(c)
