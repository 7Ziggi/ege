from itertools import product

a = set()
for i in product('ABCD', repeat=4):
    s = ''.join(i)
    a.add(''.join(sorted(s)))

print(len(a))
