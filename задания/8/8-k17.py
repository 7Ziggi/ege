from itertools import product

c = 1
for i in product('ЕЛМУР', repeat=4):
    s = ''.join(i)
    if s[0] == 'Л':
        print(c)
        break
    c += 1
