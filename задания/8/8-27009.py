from itertools import *

c = 0
for i in product('НИКОЛАЙ', repeat=4):
    s = ''.join(i)
    if (s[0] != 'Й') and (s.count('И') + s.count('А') + s.count('О')) >= 1:
        c += 1
print(c)
