from itertools import *

c = 0
for i in product("АВСХ", repeat=5):
    s = ''.join(i)
    if ((s[0] in 'Х') and (s.count('Х') == 1)) or (s.count('Х') == 0):
        c += 1
print(c)
