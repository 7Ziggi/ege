from itertools import product

c = 0
for i in product('ВИШНЯ', repeat=6):
    s = ''.join(i)
    if (s.count('В') <= 1) and (s[0] != 'Ш') and (not(s[-1] in 'ИЯ')):
        c += 1
print(c)
