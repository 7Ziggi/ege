from itertools import *

c = 1
for i in product('AПРСУ', repeat=4):
    s = ''.join(i)
    if s.count('A') == 0:
        print(c)
        break
    c += 1
