from itertools import product

c = 1
m = 0
for i in product(sorted('АЛГОРИТМ'), repeat=4):
    s = ''.join(i)
    if s[-2:] == 'ИМ':
        m = c
    c += 1

print(m)
