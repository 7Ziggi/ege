from itertools import *

c = 0
iskl = ['УА', 'АУ', 'АА', 'УУ']
for i in permutations('РУСЛАН', r=6):
    s = ''.join(i)
    # if all(j not in s for j in iskl):
    #     c += 1

    flag = True
    for j in iskl:
        if j in s:
            flag = False
    if flag:
        c += 1
print(c)
