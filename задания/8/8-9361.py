from itertools import *

c = 0
for i in product("ПИР", repeat=5):
    s = ''.join(i)
    if s.count('П') == 1:
        c += 1
print(c)
