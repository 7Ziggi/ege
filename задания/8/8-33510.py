from itertools import *

c = 0
for i in product('ТИМОФЕЙ', repeat=5):
    s = ''.join(i)
    if s.count('Т') >= 1 and s.count('Й') <= 1:
        c += 1
print(c)