from itertools import *

c = 0
for i in product("ВОЛК", repeat=5):
    s = ''.join(i)
    if s.count('В') == 1:
        c += 1

print(c)
