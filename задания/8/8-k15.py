from itertools import permutations

c = 0
w = ['ВФ', 'ФВ']

for i in permutations('ВАЙФУ', r=4):
    s = ''.join(i)
    if s[0] != 'Й' and all(j not in s for j in w):
        c += 1

print(c)
