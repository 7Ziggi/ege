from itertools import product

c = 0
for i in product('0123456789', repeat=3):
    s = ''.join(i)
    if s[0] != '0':
        if list(s) == sorted(s):
            c += 1

print(c)
