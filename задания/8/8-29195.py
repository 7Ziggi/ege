from itertools import *

c = 0
for i in product('РЕГИНА', repeat=5):
    s = ''.join(i)
    if s.count('Р') == 1 and s.count('Г') == 1 and s.count('Н') <= 1:
        c += 1
print(c)
