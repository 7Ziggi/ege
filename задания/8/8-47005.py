from itertools import *

iskl = ['ОА', 'АО', 'АА', 'ОО']
for i in product('ПРБЛ', repeat=2):
    iskl.append(''.join(i))

a = set()
for i in permutations('ПАРАБОЛА', r=8):
    s = ''.join(i)
    if all(j not in s for j in iskl):
        a.add(s)

print(len(a))
