from itertools import *

c = 0
for i in product('ШКОЛА', repeat=3):
    s = ''.join(i)
    if s.count('К') == 1:
        c += 1
print(c)
