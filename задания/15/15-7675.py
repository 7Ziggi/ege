a = set()


def f(x):
    A = x in a
    Q = x in {2, 4, 6, 8, 10, 12}
    P = x in {3, 6, 9, 12, 15}
    return Q <= ((P and (not A)) <= (not Q))


for x in range(1000):
    if f(x) == 0:
        a.add(x)
print(sum(a))
