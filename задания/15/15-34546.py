from itertools import *


def f(x, a1, a2):
    p = 23 <= x <= 58
    q = 1 <= x <= 39
    a = a1 <= x <= a2
    return (p or a) <= (q or a)


Ox = [i / 4 for i in range(0 * 4, 59 * 4 + 1)]
m = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x, a1, a2) for x in Ox):
        m += [a2 - a1]

print(min(m))
print(round(min(m)))
