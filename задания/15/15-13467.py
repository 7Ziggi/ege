a = set()


def f(x):
    A = x in a
    P = x in set(range(2, 21, 2))
    Q = x in set(range(3, 30, 3))
    return (P <= A) or ((not A) <= (not Q))


for x in range(1000):
    if f(x) == 0:
        a.add(x)
print(sum(a))
