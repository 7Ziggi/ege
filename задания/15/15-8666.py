from itertools import combinations


def f(x, a1, a2):
    p = 25 <= x <= 50
    q = 32 <= x <= 47
    a = a1 <= x <= a2
    return ((not a) <= p) <= (a <= q)


Ox = [i / 4 for i in range(24 * 4, 51 * 4 + 1)]
m = []
for a1, a2 in combinations(Ox, 2):
    if all(f(x, a1, a2) for x in Ox):
        m += [a2 - a1]
print(max(m))
