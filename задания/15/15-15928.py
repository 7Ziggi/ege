from itertools import *


def f(x, a1, a2):
    a = a1 <= x <= a2
    return (a <= (x ** 2 <= 81)) and ((x ** 2 <= 36) <= a)


Ox = [i / 4 for i in range(-11 * 4, 11 * 4 + 1)]
m = []

for a1, a2 in combinations(Ox, 2):
    if all(f(x, a1, a2) for x in Ox):
        m += [a2 - a1]

print(max(m))
