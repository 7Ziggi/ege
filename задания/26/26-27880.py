f = open("txt\\26-27880.txt")
s = int(f.readline().split()[0])
a = sorted([int(i) for i in f])
c = 0
p = 0
m = 0

for i in range(len(a)):
    if s - a[i] >= 0:
        s -= a[i]
        c += 1
    else:
        s += a[i]
        p = i
        break

for i in range(len(a) - 1, p, -1):
    if s - a[i] >= 0:
        s -= a[i]
        m = a[i]

print(c, m)
